package fr.imt_atlantique.fragmentstatique;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import fr.imt_atlantique.fragmentstatique.EditFragment;
import fr.imt_atlantique.fragmentstatique.DisplayFragment;

public class MainActivity extends AppCompatActivity implements EditFragment.OnEditNameInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public void onEditName(String name) {
        DisplayFragment displayFragment = (DisplayFragment) getSupportFragmentManager().findFragmentById(R.id.DisplayFragment);
        displayFragment.displayName(name);
    }
}